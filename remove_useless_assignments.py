from sys import stdin
from sys import stdout


def parse(program):
    def parse_operator(i, operator):
        if operator[0] == '#':
            return ['#']
        if operator[0] == '>':
            return ['>', operator[1]]
        if operator[0] == '<':
            return ['<', operator[1]]
        if operator[0] == ':':
            return [':', int(operator[1:]) - 1]
        if operator[0] == '?':
            return ['?', operator[1], int(operator[2:]) - 1]
        return ['=', operator[0], list(operator[2:])]

    return {i: parse_operator(i, op) for i, op in enumerate(program.strip().split('\n'))}


def remove_useless_assignments(program):
    def compute_out_live_vars(program):
        def get_referenced_vars(operator):
            if operator[0] == '>':
                return {operator[1]}
            if operator[0] == '?':
                return {operator[1]}
            if operator[0] == '=':
                return set(operator[2])
            return set()

        def get_defined_vars(operator):
            if operator[0] == '<':
                return {operator[1]}
            if operator[0] == '=':
                return {operator[1]}
            return set()

        def compute_successors(program):
            successors = {}
            for i, operator in program.items():
                successors[i] = set()

                if operator[0] == ':':
                    successors[i].add(operator[1])

                if operator[0] == '?':
                    successors[i].add(operator[2])

                if operator[0] not in ('#', ':'):
                    successors[i].add(i + 1)

            return successors

        successors = compute_successors(program)

        all_out_live_vars = {}
        all_in_live_vars = {i: set() for i in program}
        while True:
            new = False
            for i, operator in program.items():
                out_live_vars = all_out_live_vars[i] = set()
                for j in successors[i]:
                    out_live_vars |= all_in_live_vars[j]
                defined_vars = get_defined_vars(operator)
                referenced_vars = get_referenced_vars(operator)
                in_live_vars = (out_live_vars - defined_vars) | referenced_vars
                new |= (in_live_vars != all_in_live_vars[i])
                all_in_live_vars[i] = in_live_vars
            if not new:
                break

        return all_out_live_vars

    out_live_vars = compute_out_live_vars(program)

    new_program = {}

    for i, operator in program.items():
        if operator[0] == '=' and operator[1] not in out_live_vars[i]:
            continue
        new_program[i] = operator

    return new_program


def compact(program):
    mapping = {o: n for n, o in enumerate(sorted(program))}
    for i in range(max(mapping)):
        if i not in mapping:
            mapping[i] = mapping[min(j for j in mapping if j > i)]
    
    new_program = {}
    for i, operator in program.items():
        if operator[0] == ':':
            operator = [':', mapping[operator[1]]]
        elif operator[0] == '?':
            operator = ['?', operator[1], mapping[operator[2]]]
        new_program[mapping[i]] = operator
    program = new_program

    return program


def render(program):
    def render_operator(operator):
        if operator[0] == '#':
            return '#'
        if operator[0] == '>':
            return '>' + operator[1]
        if operator[0] == '<':
            return '<' + operator[1]
        if operator[0] == ':':
            return ':' + str(operator[1] + 1)
        if operator[0] == '?':
            return '?' + operator[1] + str(operator[2] + 1)
        return operator[1] + '=' + ''.join(operator[2])

    return '\n'.join(render_operator(program[i]) for i in sorted(program))


program = stdin.read()

program = parse(program)
program = remove_useless_assignments(program)
program = compact(program)
program = render(program)

stdout.write(program)
stdout.write('\n')


